var legion; 
var objectStore;
var messageAPI;
var userId = makeid();
var turn = 0;
var myTurn = -1;
var userName = "";
var otherPlayer = null;
var otherUsername = null;
var busy = false;


setTimeout(function () {
            showPopup();
}, 1);

function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}

require("https://legion.di.fct.unl.pt/applications/legion-min.js");


//$.getScript("https://legion.di.fct.unl.pt/applications/legion-min.js", function(){

    var options = {
        overlayProtocol: {
            type: CliquesOverlay,
            parameters: {floodInterval: 7*1000} //cada X segundos tenta ligar a outros nós.
        },
        signallingConnection : {
                type: ServerConnection,
                server: {ip: "legion.di.fct.unl.pt", port: 443}
            },
        objectServerConnection : {
                type: ObjectServerConnection,
                server: {ip: "legion.di.fct.unl.pt", port: 8000}
            },
        securityProtocol : SecurityProtocol
    };

    legion = new Legion(options);


    legion.overlay.setOnChange(function (change, peerIds, serverIds) {
        var network = $("#network_status");
        var pc = $("#network_status_peerCount")[0];
        //var sc = $("#network_status_server")[0];
        if (peerIds.length == 0 && serverIds.length == 0) {
            network.removeClass("panel-success");
            network.addClass("panel-danger");
            pc.innerText = 0;
            //sc.innerText = 0;
        } else {
            network.removeClass("panel-danger");
            network.addClass("panel-success");
            pc.innerText = "(" + peerIds.length + ") " + peerIds;
            if (serverIds.length > 0) {
               // sc.innerText = "Connected to [" + serverIds + "].";
            } else {
               // sc.innerText = "Disconnected.";
            }
        }
    });

    console.log(legion.peer);
    console.log(userId);

    messageAPI.setHandlerFor("Ok", function(message){
        otherPlayer = message.data.from;
        otherUsername = message.name;
        document.getElementById('p2').value = otherUsername;
    });

    messageAPI.setHandlerFor("Me", function(message){
        if(otherPlayer == null){
            myTurn = message.data.value;
            otherPlayer = message.data.from;
            console.log("done; myTurn = "+myTurn);
            legion.generateMessage("Ok", {value: userId, from:legion.id, name:userName}, function(result){
                messageAPI.sendTo(message.data.from, result);
            });
            otherUsername = message.name;
            document.getElementById('p2').value = otherUsername;
        }
    });

    messageAPI.setHandlerFor("WhosThere", function(message){
        if(otherPlayer==null && legion.id+0<message.data.from+0){
            var r = Math.random();
            if(r<0.5)
                myTurn = 0;
            else
                myTurn = 1;
            console.log("turn = "+myTurn);
            legion.generateMessage("Me", {value: (myTurn+1)%2, from:legion.id, name: userName}, function(result){
                messageAPI.sendTo(message.data.from, result);
            });
        }
    });


    messageAPI.setHandlerFor("Semaphore", function(message){
        console.log(message);
        console.log(userId);
        console.log(message.data.from);
        console.log("click bait");
        if(message.data.from != userId){
            clicked_id = message.data.value;
            buttonClick(clicked_id, message.data.from);
        }
    });


var field = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];

console.log(field[0][0]);

function win(color) {
    
    var winArray = [color, color, color],
        winBool = false,
        colArray = [],
        i, j,
        row;
    
    for (i = 0; i < 4; i++) { 
        for (j = 0; j < 3; j++){
            colArray.push(field[j][i]);
            if (arrayContainsSub(field[j], winArray)) {
                winBool = true;
            }
        }
        if (arrayContainsSub(colArray, winArray)) {
            winBool = true;
        }
        colArray = [];
    }

    var crossArrayRight = [];
    var crossArrayLeft = [];
    for(i = 0; i <= 1; i++){
        for (j = 0; j < 3; j++) {
            crossArrayRight[j] = field[j][j + i];
            crossArrayLeft[j] = field[j][2 - j + i];

        }
        if(arrayContainsSub(crossArrayLeft, winArray)
                || arrayContainsSub(crossArrayRight, winArray)) {
            winBool = true;
        }
    }

    return winBool;
    
}

function winAlert(player){
    if(player == userId)
        alert("win");
    else
        alert("lost");
    clean();
}

function buttonClick(clicked_id, player = userId) {

    if(otherPlayer == null) return;

    if(player==userId){
        if(turn != myTurn || turn==-1) return;
        legion.generateMessage("Semaphore", {value: clicked_id, from:userId}, function(result){
            messageAPI.sendTo(otherPlayer, result);
        });
    }
    
    console.log(clicked_id);
    //11 row 1 col 1
    var row = Math.floor(clicked_id / 10) - 1,
        col = clicked_id % 10 - 1;
    
    if (field[row][col] === 0) {
        document.getElementById(clicked_id).style.background = 'green';
        field[row][col] += 1;
    } else if (field[row][col] === 1) {
        document.getElementById(clicked_id).style.background = 'yellow';
        field[row][col] += 1;
    } else if (field[row][col] === 2) {
        document.getElementById(clicked_id).style.background = 'red';
        field[row][col] += 1;
    }  

    if(win(field[row][col])){
        setTimeout(function () {
            winAlert(player);
        }, 1);
        myTurn = (myTurn+1)%2;
    }
    else{
        turn = (turn+1)%2;
    }
}

function clean(){
    var i, j;
    for(i = 1; i<=4; i++){
        for(j = 1; j<=3; j++){
            var id = ""+j+""+i;
            document.getElementById(id).style.background = 'lightyellow';
        }
    }
    field = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
}


function arrayContainsSub(arr, sub) {
    var first = sub[0],
        i = 0,
        starts = [];

    while (arr.indexOf(first, i) >= 0) {
        starts.push(arr.indexOf(first, i));
        i = arr.indexOf(first, i) + 1;
    }

    return !!starts
        .map(function (start) {
            for (var i = start, j = 0; j < sub.length; i++, j++) {
                if (arr[i] !== sub[j]) {
                    return false;
                }
                if (j === sub.length - 1 && arr[i] === sub[j]) {
                    return true;
                }
            };

    }).filter(function(res) {
        return res;
    }).length;
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


function showPopup() {
    var popup = document.getElementById('myPopup');
    popup.classList.toggle('show');
}

function hidePopup() {
    var popup = document.getElementById('myPopup');
    //popup.classList.toggle('hide');
    userName = document.getElementById('UserNameInput').value;
    popup.style.display='none';
    document.getElementById('p1').innerHTML = userName + '<span id="p2" style="float:right;">waiting for other player...</span>' ;
    var groupOptions = {
        id: "default", secret: "default"               //ex: chat rooms with login keys
    };
    legion.joinGroup(groupOptions, function (group) {
        objectStore = legion.getObjectStore();
        messageAPI = legion.getMessageAPI();

        function whosthere(){
            messageAPI.broadcast("WhosThere", {value: userId, from:legion.id});
        }
        
        whosthere();
        var timer = setInterval(function () {
            if(otherPlayer!=null)
                clearInterval(timer);
            else
                whosthere();
        }, 3000);
    });
	
	legion.onJoin(function(){
	
		
	});
}


